@echo off

REM ####################################################################
REM #                                                                  #
REM #                                                                  #
REM #  P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T   #
REM #                                                                  #
REM #            Copyright 2019 by PB-Soft / Patrick Biegel            #
REM #                                                                  #
REM #                           Version 1.2                            #
REM #                                                                  #
REM #                       https://pb-soft.com                        #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  This tool helps to find different devices in the local network  #
REM #  and shows their IP and MAC addresses and for some devices also  #
REM #  the vendor (Raspberry Pi / QNAP / Synology). The script first   #
REM #  checks the ARP table and then is paused. If your devices are    #
REM #  not listed you can start the extended search (which takes much  #
REM #  longer to complete) but this one will ping the devices with the #                                                                #
REM #  ARP protocol and therefore (hopefully) will find most of them.  #
REM #  If a device is found, the script will ping it the 'normal' way  #
REM #  (ICMP) and check if there is an answer and report it.           #
REM #                                                                  #
REM #  This script uses the free 'arp-ping.exe' executable developed   #
REM #  by 'Eli Fulkerson' - https://elifulkerson.com - and released    #
REM #  under GPL. Thanks a lot for this nice tool which is available   #
REM #  from the following website:                                     #
REM #                                                                  #
REM #          https://elifulkerson.com/projects/arp-ping.php          #
REM #                                                                  #
REM ####################################################################


REM ====================================================================
REM               C O N F I G U R A T I O N  -  B E G I N
REM ====================================================================

REM Organizationally Unique Identifiers - OUI
REM Available at: https://linuxnet.ca/ieee/oui/
REM
REM - Raspberry Pi: B8:27:EB / DC:A6:32
REM - QNAP / ICP:   24:5E:BE / E8:43:B6 / 00:08:9B
REM - Synology:     00:11:32

REM Specify the network address without the last number.
REM Netmask will be: 255.255.255.0
SET NETWORK=192.168.0

REM Specify the search range.
SET RANGE_BEGIN=1
SET RANGE_END=20

REM Specify the quick search patterns.
SET Q_RASPBERRY_1_PATTERN=b8-27-eb
SET Q_RASPBERRY_2_PATTERN=dc-a6-32
SET Q_QNAP_1_PATTERN=24-5e-be
SET Q_QNAP_2_PATTERN=e8-43-b6
SET Q_QNAP_3_PATTERN=00-08-9b
SET Q_SYNOLOGY_PATTERN=00-11-32

REM Specify the extended search patterns.
SET E_RASPBERRY_1_PATTERN=B8:27:EB
SET E_RASPBERRY_2_PATTERN=DC:A6:32
SET E_QNAP_1_PATTERN=24:5E:BE
SET E_QNAP_2_PATTERN=E8:43:B6
SET E_QNAP_3_PATTERN=00:08:9B
SET E_SYNOLOGY_PATTERN=00:11:32

REM Specify if unknown devices should be displayed.
SET UNKNOWN=1

REM Specify if all IP addresses should be displayed during the search.
SET DISPLAY_ALL_IP=1


REM ====================================================================
REM                 C O N F I G U R A T I O N  -  E N D
REM ====================================================================

REM Specify the script version.
SET VERSION=1.2


REM ====================================================================
REM                Q U I C K   D E V I C E   S E A R C H
REM ====================================================================

REM Clear the screen.
cls

REM Display some information about the script.
ECHO.
ECHO ===============================================================================
ECHO.
ECHO.         P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T
ECHO.
ECHO.                                 VERSION: %VERSION%
ECHO.
ECHO.                           Copyright 2019 by PB-Soft
ECHO.
ECHO.                              https://pb-soft.com
ECHO.
ECHO ===============================================================================
ECHO.
ECHO           P B D E V I C E   -   Q U I C K   D E V I C E   S E A R C H
ECHO.
ECHO ===============================================================================
ECHO.

REM Check the ARP table for Raspberry Pi devices.
ECHO  Raspberry Pi devices found:
ECHO.
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_RASPBERRY_1_PATTERN%') do @echo  IP: %%a / MAC: %%b
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_RASPBERRY_2_PATTERN%') do @echo  IP: %%a / MAC: %%b
ECHO.
ECHO.

REM Check the ARP table for QNAP devices.
echo  QNAP devices found:
ECHO.
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_QNAP_1_PATTERN%') do @echo  IP: %%a / MAC: %%b
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_QNAP_2_PATTERN%') do @echo  IP: %%a / MAC: %%b
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_QNAP_3_PATTERN%') do @echo  IP: %%a / MAC: %%b
ECHO.
ECHO.

REM Check the ARP table for Synology devices.
ECHO  Synology devices found:
ECHO.
FOR /f "tokens=1,2" %%a in ('arp -a ^| findstr /i %Q_SYNOLOGY_PATTERN%') do @echo  IP: %%a / MAC: %%b
ECHO.

REM Display the final bar.
ECHO ===============================================================================
ECHO.

REM Pause the execution of the script.
ECHO  -^> Please press 'any key' if you want to start the extended search.
ECHO.
ECHO  -^> Please press 'CONTROL + C' if you want to exit the script.
ECHO.
PAUSE>NUL

REM ====================================================================
REM             E X T E N D E D   D E V I C E   S E A R C H
REM ====================================================================

REM Clear the screen.
cls

REM Display an information message.
ECHO.
ECHO ===============================================================================
ECHO.
ECHO        P B D E V I C E   -   E X T E N D E D   D E V I C E   S E A R C H
ECHO.
ECHO ===============================================================================
ECHO.
ECHO  Searching the network %NETWORK%.0 for devices...
ECHO.

REM Loop through the IP address range.
SETLOCAL EnableDelayedExpansion
for /l %%x in (%RANGE_BEGIN%, 1, %RANGE_END%) do (

    REM Check if all IP addresses should be displayed during the search.
    IF %DISPLAY_ALL_IP%==1 (

        REM Display an information message.
        ECHO.
        ECHO  Searching for a device at IP %NETWORK%.%%x...
    )

    REM Initialize the output variable.
    SET OUTPUT=0

    REM Check if the actual host is available in the network.
    for /f "tokens=3,5" %%a in ('arp-ping -x %NETWORK%.%%x ^| findstr /i /c:"Reply that"') do (

        REM Get the IP address of the actual host.
        SET IP=%%b

        REM Get the MAC address of the actual host.
        SET MAC=%%a

        REM Search for Raspberry Pi devices - Pattern 1.
        ECHO.!MAC! | findstr /C:"!E_RASPBERRY_1_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / Raspberry Pi
        )

        REM Search for Raspberry Pi devices - Pattern 2.
        ECHO.!MAC! | findstr /C:"!E_RASPBERRY_2_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / Raspberry Pi
        )

        REM Search for QNAP devices - Pattern 1.
        ECHO.!MAC! | findstr /C:"!E_QNAP_1_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / QNAP device
        )

        REM Search for QNAP devices - Pattern 2.
        ECHO.!MAC! | findstr /C:"!E_QNAP_2_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / QNAP device
        )

        REM Search for QNAP devices - Pattern 3.
        ECHO.!MAC! | findstr /C:"!E_QNAP_3_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / QNAP device
        )

        REM Search for Synology devices.
        ECHO.!MAC! | findstr /C:"!E_SYNOLOGY_PATTERN!" 1>nul

        REM Check if a device was found.
        IF NOT ERRORLEVEL 1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / Synology device
        )

        REM Check if a unknown device should be displayed.
        IF !OUTPUT!==0 IF !UNKNOWN!==1 (

            REM Show the IP / MAC addresses and the type of the device.
            SET OUTPUT=IP: !IP! / MAC: !MAC! / Unknown device
        )
    )

    REM Check if a device was found.
    IF NOT !OUTPUT!==0 (

        REM Check if the device answers a ICMP ping request.
        ping -n 1 %NETWORK%.%%x | find "TTL=" >nul

        REM Check if the ICMP ping was successful.
        IF NOT ERRORLEVEL 1 (

            REM Show that the ICMP ping request was answered.
            ECHO  !OUTPUT! / Ping OK

        REM The divice did not answer the ICMP ping request.
        ) ELSE (

            REM Show that the ICMP ping request has failed.
            ECHO  !OUTPUT! / Ping --
        )
    )
)
ENDLOCAL

REM Display the final bar.
ECHO.
ECHO ===============================================================================
ECHO.

REM Pause the execution of the script.
ECHO  -^> Please press 'any key' to exit the script.
ECHO.
PAUSE>NUL
