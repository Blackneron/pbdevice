#!/bin/bash

########################################################################
#                                                                      #
#                                                                      #
#    P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T     #
#                                                                      #
#              Copyright 2019 by PB-Soft / Patrick Biegel              #
#                                                                      #
#                             Version 1.2                              #
#                                                                      #
#                         https://pb-soft.com                          #
#                                                                      #
########################################################################
#                                                                      #
#  This tool helps to find different devices from specified vendors    #
#  (Raspberry Pi / QNAP / Synology) in the local network and shows     #
#  their IP and MAC addresses. The script only checks the ARP table    #
#  and therefore is very quick in displaying the devices. But if your  #
#  device is not listed in the ARP table it will not be displayed. In  #
#  this case, run the script 'pbdevice_extended.sh' for an extended    #
#  search and to find more devices.                                    #
#                                                                      #
#                                                                      #
#  Before you can run this script you have to make it executable with  #
#  the following command and then start it:                            #
#                                                                      #
#    chmod +x /path/to/script/pbdevice_extended.sh                     #
#    sudo /path/to/script/pbdevice_extended.sh                         #
#                                                                      #
########################################################################


# ======================================================================
#                C O N F I G U R A T I O N  -  B E G I N
# ======================================================================

# Organizationally Unique Identifiers - OUI
# Available at: https://linuxnet.ca/ieee/oui/
#
# - Raspberry Pi: B8:27:EB / DC:A6:32
# - QNAP / ICP:   24:5E:BE / E8:43:B6 / 00:08:9B
# - Synology:     00:11:32

# Specify the quick search patterns.
RASPBERRY_1_PATTERN="b8:27:eb"
RASPBERRY_2_PATTERN="dc:a6:32"
QNAP_1_PATTERN="24:5e:be"
QNAP_2_PATTERN="e8:43:b6"
QNAP_3_PATTERN="00:08:9b"
SYNOLOGY_PATTERN="00:11:32"


# ======================================================================
#                  C O N F I G U R A T I O N  -  E N D
# ======================================================================

# Specify the script version.
VERSION="1.2"

# Clear the screen.
clear

# Display some information about the script.
echo "==============================================================================="
echo
echo "         P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T"
echo
echo "                                 VERSION:" $VERSION
echo
echo "                           Copyright 2019 by PB-Soft"
echo
echo "                              https://pb-soft.com"
echo
echo "==============================================================================="
echo
echo "          P B D E V I C E   -   Q U I C K   D E V I C E   S E A R C H"
echo
echo "==============================================================================="
echo

# Check the ARP table for Raspberry Pi devices.
echo " Raspberry Pi devices found:"
echo
arp -n | grep $RASPBERRY_1_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
arp -n | grep $RASPBERRY_2_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
echo
echo

# Check the ARP table for QNAP devices.
echo " QNAP devices found:"
echo
arp -n | grep $QNAP_1_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
arp -n | grep $QNAP_2_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
arp -n | grep $QNAP_3_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
echo
echo

# Check the ARP table for Synology devices.
echo " Synology devices found:"
echo
arp -n | grep $SYNOLOGY_PATTERN | awk -F" " '{printf(" IP:");printf $1;printf(" | MAC: ");print $3;}'
echo

# Display the final bar.
echo "==============================================================================="
echo
