#!/bin/bash

########################################################################
#                                                                      #
#    P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T     #
#                                                                      #
#              Copyright 2019 by PB-Soft / Patrick Biegel              #
#                                                                      #
#                             Version 1.2                              #
#                                                                      #
#                         https://pb-soft.com                          #
#                                                                      #
########################################################################
#                                                                      #
#  This tool helps to find different devices in the local network and  #
#  shows their IP and MAC addresses and for some devices also the      #
#  vendor (Raspberry Pi / QNAP / Synology).                            #
#                                                                      #
#  This script uses the free 'netdiscover' application developed by    #
#  'Jaime Penalba' and released under GPL3. Thanks a lot for this      #
#  nice tool which is available from the following website:            #
#                                                                      #
#                https://github.com/alexxy/netdiscover                 #
#                                                                      #
#  On a Debian based system (Debian / Mint / Ubuntu) you can install   #
#  the tool 'netdiscover' with the following commands:                 #
#                                                                      #
#    sudo apt-get update              (update the software list)       #
#    sudo apt-get upgrade             (upgrade the existing packages)  #
#    sudo apt-get install netdiscover (install the 'netdiscover' tool) #
#                                                                      #
#  After that, you should make this script executable and then run it  #
#  with root privileges:                                               #
#                                                                      #
#    chmod +x /path/to/script/pbdevice_extended.sh                     #
#    sudo /path/to/script/pbdevice_extended.sh                         #
#                                                                      #
########################################################################


# ======================================================================
#                C O N F I G U R A T I O N  -  B E G I N
# ======================================================================

# Organizationally Unique Identifiers - OUI
# Available at: https://linuxnet.ca/ieee/oui/
#
# - Raspberry Pi: b8:27:eb / dc:a6:32
# - QNAP / ICP:   24:5e:be / e8:43:b6 / 00:08:b
# - Synology:     00:11:32


# Specify the script version.
VERSION="1.2"


# ======================================================================
#                  C O N F I G U R A T I O N  -  E N D
# ======================================================================

# Initialize the match flag.
MATCH=0

# Check if a network was specified.
if [ $2 ]; then

  # Specify the network.
  NET="$2"

# The network was not specified and the default network has to be used.
else

  # Get the network information from the actual network.
  STRING=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}')
  ADDRESS=$(echo $STRING | cut -f1 -d'/')
  NETMASK=$(echo $STRING | cut -f2 -d'/')
  NETWORK=$(echo $ADDRESS | cut -f1-3 -d'.')".0"

  # Specify the default network.
  NET="$NETWORK/$NETMASK"

fi

# Function to output the available devices in the network.
function output() {

  # Display the device title.
  echo
  echo "$1"
  echo "==============================================================================="

  # Display the device list with MAC address and IP address.
  sudo netdiscover -P -N -r "$2" | grep -e "$3" | awk -F" " '{printf("MAC: ");printf $2;printf(" | IP: ");print $1}'

  # Add a space.
  echo
}

# Clear the screen.
clear

# Display some information about the script.
echo "==============================================================================="
echo
echo "         P B D E V I C E   -   S E A R C H   D E V I C E   S C R I P T"
echo
echo "                                 VERSION:" $VERSION
echo
echo "                           Copyright 2019 by PB-Soft"
echo
echo "                              https://pb-soft.com"
echo
echo "==============================================================================="
echo
echo "       P B D E V I C E   -   E X T E N D E D   D E V I C E   S E A R C H"
echo
echo "==============================================================================="
echo

# Check if no input was specified.
if [ ! "$1" ]; then

  # Display information about the usage of this script.
  echo "Usage: search_device.sh [type] [network]"
  echo
  echo "------------------------------------------------------------"
  echo
  echo "For the 'type' (mandatory) you can use the following values:"
  echo
  echo "  all:  Shows all available devices."
  echo "  comp: Shows all available devices (complete info)."
  echo
  echo "  net:  Shows all 'Netgear' devices."
  echo "  qnap: Shows all 'QNAP' devices."
  echo "  rpi:  Shows all 'Raspberry Pi' devices."
  echo "  sam:  Shows all 'Samsung' devices."
  echo "  syn:  Shows all 'Synology' devices."
  echo "  tpl:  Shows all 'TP-Link' devices."
  echo "  unk:  Shows all 'unknown' devices."
  echo
  echo "  full: Shows all the information from above."
  echo
  echo "If you enter another type of device or search term, the script"
  echo "will search for devices which match the search term."
  echo
  echo "------------------------------------------------------------"
  echo
  echo "For the 'network' (optional) you can specify the format:"
  echo
  echo "  192.168.0.0/24"
  echo
  echo "If there is no network specified, the following one was"
  echo "calculated and will be used:"
  echo
  echo "  $NET"
  echo
  echo "------------------------------------------------------------"
  echo

  # Exit the script
  exit

fi

# Check if all devices should be displayed.
if [ "$1" == "all" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="All Devices:"

  # Specify the search term.
  SEARCHTERM=":"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all devices should be displayed - Complete output.
if [ "$1" == "comp" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="All Devices Complete:"

  # Specify the search term.
  SEARCHTERM=":"

  # Display the device title.
  echo
  echo "$TITLE"
  echo "==============================================================================="
  echo

  # Display the device list with MAC address and IP address.
  sudo netdiscover -P -N -r "$NET" | grep "$SEARCHTERM"
  echo

  # Enable the match flag.
  MATCH=1

fi

# Check if all Netgear devices should be displayed.
if [ "$1" == "net" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="Netgear Devices (NETGEAR):"

  # Specify the search term.
  SEARCHTERM="NETGEAR"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all QNAP devices should be displayed.
if [ "$1" == "qnap" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="QNAP Devices (ICP Electronics Inc. | 24:5e:be | e8:43:b6 | 00:08:9b):"

  # Specify the search term.
  SEARCHTERM="ICP Electronics Inc.\|24:5e:be\|e8:43:b6\|00:08:9b"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all Raspberry Pi devices should be displayed.
if [ "$1" == "rpi" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="Raspberry Pi Devices (Raspberry Pi Foundation | b8:27:eb | dc:a6:32):"

  # Specify the search term.
  SEARCHTERM="Raspberry Pi Foundation\|b8:27:eb\|dc:a6:32"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all Samsung devices should be displayed.
if [ "$1" == "sam" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="Samsung Devices (Samsung Electronics Co.):"

  # Specify the search term.
  SEARCHTERM="Samsung Electronics Co."

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all Synology devices should be displayed.
if [ "$1" == "syn" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="Synology Devices (Synology Incorporated | 00:11:32):"

  # Specify the search term.
  SEARCHTERM="Synology Incorporated\|00:11:32"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all TP-Link devices should be displayed.
if [ "$1" == "tpl" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="TP-Link Devices (TP-LINK TECHNOLOGIES CO.):"

  # Specify the search term.
  SEARCHTERM="TP-LINK TECHNOLOGIES CO."

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if all unknown devices should be displayed.
if [ "$1" == "unk" ] || [ "$1" == "full" ]; then

  # Specify the title.
  TITLE="All Unknown Devices:"

  # Specify the search term.
  SEARCHTERM="Unknown vendor"

  # Call the function to output the search result.
  output "$TITLE" "$NET" "$SEARCHTERM"

  # Enable the match flag.
  MATCH=1

fi

# Check if the type did not match.
if [ "$MATCH" == "0" ]; then

  # Specify the title.
  TITLE="Search for devices called '$1':"

  # Specify the search term.
  SEARCHTERM="$1"

  # Display the device title.
  echo
  echo "$TITLE"
  echo "==============================================================================="
  echo

  # Display the device list with MAC address and IP address.
  sudo netdiscover -P -N -r "$NET" | grep "$SEARCHTERM"
  echo

fi
