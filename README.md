# PBdevice Script - README #
---

### Overview ###

The **PBdevice** scripts (Bash/Batch) help to find different devices in the local network and show their IP and MAC addresses and for some devices also the vendor (Raspberry Pi / QNAP / Synology). With the quick search, the scripts check the ARP table and list the devices available. If your devices are not listed after the quick search, you can use the extended search (which takes longer to complete) which makes ARP requests direct to the devices and therefore (hopefully) will find most of them. The scrips are using the following software (licensed under GPL3):

Arp-Ping from Eli Fulkerson (Batch file use): [**Arp-Ping - An implementation of ping via arp lookup**](https://elifulkerson.com/projects/arp-ping.php)

Netdiscover from Jaime Penalba (Bash file use): [**Netdiscover - Network address discovering tool**](https://github.com/alexxy/netdiscover)


### Screenshots - Windows ###

![PBdevice - Quick Search (Batch file)](development/readme/pbdevice1.png "PBdevice - Quick Search (Batch file)")
![PBdevice - Extended Search (Batch file)](development/readme/pbdevice2.png "PBdevice - Extended Search (Batch file)")

### Screenshots - Linux ###

![PBdevice - Quick Search (Bash file)](development/readme/pbdevice3.png "PBdevice - Quick Search (Bash file)")
![PBdevice - Extended Usage Info (Bash file)](development/readme/pbdevice4.png "PBdevice - Extended Usage Info (Bash file)")
![PBdevice - Extended Search - Raspberry Pi devices (Bash file)](development/readme/pbdevice5.png "PBdevice - Extended Search - Raspberry Pi devices (Bash file)")
![PBdevice - Extended Search - QNAP devices (Bash file)](development/readme/pbdevice6.png "PBdevice - Extended Search - QNAP devices (Bash file)")

### Files ###

There are three files (1 Batch script for Windows / 2 Bash scripts for Linux):

* **pbdevice.bat** - Quick- and Extended search Tool combined for use with a Windows OS (uses **arp-ping.exe**)
* **pbdevice_quick.sh** - Quick search Tool for use with a Linux OS (does not use external packages)
* **pbdevice_extended.sh** - Extended search Tool for use with a Linux OS (uses **netdiscover**)

### Setup ###

* Copy the directory **pbdevice** to your Windows computer.
* Make the Bash scripts executable with the command: chmod +x <filename>.
* Execute the scripts (Batch on Windws / Bash on Linux).
* Check for more information (installation/configuration) in the script header.

### Support ###

This are free tools and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBdevice** scripts are licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
